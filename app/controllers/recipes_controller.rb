class RecipesController < ApplicationController
  before_action :find_recipes, only: [:show, :edit, :update, :destroy, :like]

  def index
    @recipes = Recipe.paginate(page: params[:page], per_page: 4)
  end

  def new
    @recipe = Recipe.new
  end

  def show
  end

  def create
    @recipe = Recipe.new(recipe_params)
    @recipe.chef = Chef.find(2)

    if @recipe.save
      flash[:success] = "Your recipe has been created successfully"
      redirect_to recipes_path

    else
      render :new
    end
  end

  def edit
  end

  def update
    if @recipe.update(recipe_params)
      flash[:success] = "Recipe updated successfully!"
      redirect_to recipe_path(@recipe)
    else
      render :edit
    end
  end

  def destroy
  end

  def like
    like = Like.create(like: params[:like], chef: Chef.second, recipe: @recipe)
    if like.valid?
      flash[:success] = "You have liked #{@recipe.name}"
      redirect_to :back
    else
      flash[:danger] = "You can only like/dislike a recipe once."
      redirect_to :back
    end
  end

  private

    def find_recipes
      @recipe = Recipe.find(params[:id])
    end

    def recipe_params
      params.require(:recipe).permit(:name, :summary, :description, :picture)
    end

end
